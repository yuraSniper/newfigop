#pragma once

#include <common.h>
#include <Main\Main.h>
#include <Window\Window.h>

class SelectFileDialog
{
public:
	static string OpenFile(char * filter)
	{
		char filename[2048] = "";
		OPENFILENAME of = {0};
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = Main::getInstance().mainWindow->getHWND();
		of.lpstrFilter = filter;
		of.nFilterIndex = 1;
		of.lpstrFile = filename;
		of.nMaxFile = 2048;
		of.lpstrInitialDir = "";
		of.lpstrTitle = "Open";
		of.Flags = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_LONGNAMES | OFN_PATHMUSTEXIST;
		of.lpstrDefExt = "";

		if (GetOpenFileName(&of))
			return string(filename);

		return "";
	}

	static string SaveFile(char * filter)
	{
		char filename[2048] = "";
		OPENFILENAME of = {0};
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = Main::getInstance().mainWindow->getHWND();
		of.lpstrFilter = filter;
		of.nFilterIndex = 1;
		of.lpstrFile = filename;
		of.nMaxFile = 2048;
		of.lpstrInitialDir = "";
		of.lpstrTitle = "Save";
		of.Flags = OFN_DONTADDTORECENT | OFN_LONGNAMES | OFN_PATHMUSTEXIST;
		of.lpstrDefExt = "";

		if (GetSaveFileName(&of))
			return string(filename);
		return "";
	}
};