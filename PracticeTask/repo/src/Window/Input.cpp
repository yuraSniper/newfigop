#include "Input.h"
#include "Window.h"

void Input::OnKey(char key)
{
	unsigned char k = key;
	switch (k)
	{
		case VK_SHIFT :
			k = GetAsyncKeyState(VK_LSHIFT) ? VK_LSHIFT : VK_RSHIFT;
			break;
		case VK_CONTROL:
			k = GetAsyncKeyState(VK_LCONTROL) ? VK_LCONTROL : VK_RCONTROL;
			break;
		case VK_MENU:
			k = GetAsyncKeyState(VK_LMENU) ? VK_LMENU : VK_RMENU;
			break;
	}
	if ((GetKeyState(k) >> 7) != 0)
	{
		lastKey = k;
		keyPressedState[k] = true;
	}
}

void Input::OnMouseWheel(int d)
{
	dw += d;
}

void Input::OnChar(int c)
{
	lastChar = c;
}

Input::Input(shared_ptr<Window> w)
{
	wnd = w;
	wnd->onKey.Add(this, &Input::OnKey);
	wnd->onMouseWheel.Add(this, &Input::OnMouseWheel);
	wnd->onChar.Add(this, &Input::OnChar);

	dw = 0;
	lastChar = 0;

	for (short i = 0; i < 256; i++)
	{
		keyPressedState[i] = false;
	}
}

bool Input::GetKeyPressed(unsigned char key)
{
	if (GetForegroundWindow() != wnd->GetHWND())
		return false;

	bool b = GetKey(key);

	if (!b && keyPressedState[key])
	{
		keyPressedState[key] = false;
		return true;
	}

	return false;
}

bool Input::GetKey(unsigned char key)
{
	if (GetForegroundWindow() != wnd->GetHWND())
		return false;

	bool b = (GetAsyncKeyState(key) >> 15) != 0;
	keyPressedState[key] |= b;

	return b;
}

unsigned char Input::GetLastKeyDown()
{
	unsigned char tmp = lastKey;
	lastKey = 0;
	return tmp;
}

unsigned char Input::GetLastKeyPressed()
{
	unsigned char tmp = lastKey;

	if (GetKey(lastKey))
		return 0;

	lastKey = 0;
	return tmp;
}

unsigned int Input::GetLastChar()
{
	unsigned int tmp = lastChar;
	lastChar = 0;
	return tmp;
}

bool Input::GetMouseButton(Mouse button)
{
	return GetKey(button);
}

bool Input::GetMouseButtonPressed(Mouse button)
{
	return GetKeyPressed(button);
}

void Input::SetClientCursorPos(int x, int y)
{
	POINT p = {x, y};
	ClientToScreen(wnd->GetHWND(), &p);
	SetScreenCursorPos(p.x, p.y);
}

void Input::SetScreenCursorPos(int x, int y)
{
	SetCursorPos(x, y);
}

iv2 Input::GetMousePos()
{
	iv2 p;
	GetCursorPos((LPPOINT)&p);
	ScreenToClient(wnd->GetHWND(), (LPPOINT)&p);
	return p;
}

int Input::GetMouseWheel()
{
	int tmp = dw;
	dw = 0;
	return tmp;
}