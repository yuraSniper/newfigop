#pragma once

#include <common.h>
#include <Thread\Thread.h>
#include <Window\Window.h>
#include <Window\Input.h>
#include <Shader\ShaderProgram.h>

#include <Interpreter\Interpreter.h>

class Main
{
	static bool working;
	static shared_ptr<Thread> renderThread;
	static shared_ptr<Window> mainWindow;
	static shared_ptr<Input> input;
	static shared_ptr<ShaderProgram> postProcessShader;

	static string ASTToString(shared_ptr<ASTNode> root);
	static void MainLoop();
	static void onClose();

	static void Interpret();
public:
	static shared_ptr<ShaderProgram> combineFiguresShader;
	static shared_ptr<ShaderProgram> drawFigureShader;
	static unsigned int resultTex;
	static unsigned int fbo;
	static unsigned int rectVbo;

	static void InitGL();
	static void DrawFrame();

	static void Start(vector<string> args);
};