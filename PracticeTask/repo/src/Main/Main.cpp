#include "Main.h"

#include <Main\Thread\RenderThread.h>
#include <glm.h>

bool Main::working;
shared_ptr<Thread> Main::renderThread;
shared_ptr<Window> Main::mainWindow;
shared_ptr<Input> Main::input;
shared_ptr<ShaderProgram> Main::postProcessShader;
shared_ptr<ShaderProgram> Main::combineFiguresShader;
shared_ptr<ShaderProgram> Main::drawFigureShader;
unsigned int Main::resultTex;
unsigned int Main::fbo;
unsigned int Main::rectVbo;

string Main::ASTToString(shared_ptr<ASTNode> root)
{
	string output = "";

	struct stuff
	{
		shared_ptr<ASTNode> node;
		int indent;
	};

	vector<stuff> list;
	list.push_back({root, 0});

	while (!list.empty())
	{
		stuff current = list[0];
		list.erase(list.begin());

		output.insert(output.end(), current.indent, '\t');
		output.append(current.node->ToString() + "\n");

		for (int i = 0; i < current.node->childs.size(); i++)
			list.insert(list.begin() + i, {current.node->childs[i], current.indent + 1});
	}

	return output;
}

void Main::MainLoop()
{
	working = true;

	while (working)
	{
		if (!mainWindow->MessageLoop())
			working = false;
		Sleep(1);
	}
}

void Main::onClose()
{
	working = false;
}

void Main::Interpret()
{
	string filename;
	filename = "data\\test\\simpleCode.txt";

	Lexer lexer(filename);
	Token token = lexer.NextToken();

	ofstream file;
	file.open("OutputTokenStream.txt", ios::out);

	while (token.type != TokenTypes::Eof)
	{
		//cout << token.ToString() << endl;
		file << token.ToString() << endl;

		token = lexer.NextToken();
	}

	file.close();

	shared_ptr<Program> program = Interpreter::PrepareProgram(filename);
	
	if (program != nullptr)
	{
		Interpreter interpreter;
		interpreter.Interpret(program);
		shared_ptr<ASTNode> root = Parser::Parse(filename);

		if (root != nullptr)
		{
			string output = "";
			string output2 = "";

			output = ASTToString(root);
			Parser::Simplify(root);
			output2 = ASTToString(root);

			file.open("OutputCST.txt", ios::out);
			file << output;
			file.close();

			file.open("OutputAST.txt", ios::out);
			file << output2;
			file.close();
		}
	}
}

void Main::InitGL()
{
	wglMakeCurrent(mainWindow->GetHDC(), wglCreateContext(mainWindow->GetHDC()));

	glewInit();

	cout << glGetString(GL_VERSION) << endl;
	cout << glGetString(GL_VENDOR) << endl;
	cout << glGetString(GL_RENDERER) << endl;
	cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	glClearColor(0, 0, 0, 0);

	mat4 projection = ortho<float>(0, 1, 0, 1);

	drawFigureShader = ShaderProgram::CreateFromFile("data\\Shaders\\DrawFigure.glsl");

	combineFiguresShader = ShaderProgram::CreateFromFile("data\\Shaders\\CombineFigures.glsl");
	combineFiguresShader->Bind();
	glUniform1i(combineFiguresShader->GetUniformLocation("left"), 1);
	glUniform1i(combineFiguresShader->GetUniformLocation("right"), 2);
	glUniformMatrix4fv(combineFiguresShader->GetUniformLocation("mvp"), 1, false, value_ptr(projection));

	postProcessShader = ShaderProgram::CreateFromFile("data\\Shaders\\PostProcess.glsl");
	postProcessShader->Bind();
	glUniform1i(postProcessShader->GetUniformLocation(""), 0);
	glUniformMatrix4fv(postProcessShader->GetUniformLocation("mvp"), 1, false, value_ptr(projection));
	glUseProgram(0);

	float rect[] =
	{
		0, 1, 0, 1,
		0, 0, 0, 0,
		1, 1, 1, 1,
		1, 0, 1, 0,
	};

	glGenFramebuffers(1, &fbo);

	glGenTextures(1, &resultTex);
	glBindTexture(GL_TEXTURE_2D, resultTex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, nullptr);

	glGenBuffers(1, &rectVbo);
	glBindBuffer(GL_ARRAY_BUFFER, rectVbo);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(float) * 4, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(float) * 4, (void *)(sizeof(float) * 2));

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * 4, rect, GL_STATIC_DRAW);
}

void Main::DrawFrame()
{
	int width = mainWindow->GetClientWidth();
	int height = mainWindow->GetClientHeight();

	if (width == 0 || height == 0)
		return;

	if (input->GetKeyPressed(VK_F5))
	{
		Interpret();
	}
	
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, resultTex);

	postProcessShader->Bind();

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	mainWindow->SwapBuffers();
}

void Main::Start(vector<string> args)
{
	mainWindow = make_shared<Window>(512, 512);
	mainWindow->onCLose.Add(&Main::onClose);
	mainWindow->SetVisibility(true);

	input = make_shared<Input>(mainWindow);

	renderThread = make_shared<RenderThread>();
	renderThread->resume();

	MainLoop();
}