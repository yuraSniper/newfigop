#pragma once

#include <Windows.h>
#include <windowsx.h>

#define _USE_MATH_DEFINES

#include <algorithm>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>
#include <string>
#include <typeinfo>
#include <stack>
#include <set>
#include <cmath>

#include <Lib\Glew\glew.h>
#include <Lib\Glew\wglew.h>

using namespace std;