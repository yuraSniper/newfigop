#pragma once

#include <vector>
#include <memory>
using namespace std;

template<class ... ARGS>
class Delegate
{
	struct Tmp
	{
		virtual void invoke(ARGS ... args) = 0;
	};

	template<class T>
	struct Method : public Tmp
	{
		void (T::*f)(ARGS ...);
		T * obj;
		Method(void(T::*f)(ARGS ...), T * param)
		{
			this->f = f;
			this->obj = param;
		}

		virtual void invoke(ARGS ... args)
		{
			(obj->*f)(args ...);
		}
	};

	struct Function : public Tmp
	{
		void (*f)(ARGS ...);

		Function(void(*f)(ARGS ...))
		{
			this->f = f;
		}

		virtual void invoke(ARGS ... args)
		{
			f(args ...);
		}
	};

	vector<unique_ptr<Tmp>> list;
public:
	template<class T>
	void Add(T * param, void (T::*f)(ARGS ...))
	{
		list.push_back(make_unique<Method<T>>(f, param));
	}

	void Add(void (*f)(ARGS ...))
	{
		list.push_back(make_unique<Function>(f));
	}

	void operator()(ARGS ... args)
	{
		for (int i = 0; i < list.size(); i++)
			list[i]->invoke(args ...);
	}

	void Clear()
	{
		list.clear();
	}
};