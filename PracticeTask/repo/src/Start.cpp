#include <common.h>

#include <Main\Main.h>

void main(int argc, char * argv[])
{
	vector<string> args;

	for (int i = 0; i < argc; i++)
		args.push_back(argv[i]);

	Main::Start(args);
}