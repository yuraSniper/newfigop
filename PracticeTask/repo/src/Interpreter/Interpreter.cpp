#include "Interpreter.h"

#include <Main\Main.h>

shared_ptr<Procedure> Interpreter::createTriangle;
shared_ptr<Procedure> Interpreter::createRect;
shared_ptr<Procedure> Interpreter::createCircle;

void Interpreter::RunProcedure(shared_ptr<Procedure> procedure, int stackFrameBeginning)
{
	EvaluateStatement(procedure->body, procedure->scope, stackFrameBeginning);
}

BreakType Interpreter::EvaluateStatement(shared_ptr<ASTNode> statement, shared_ptr<Scope> scope, int stackFrameBeginning)
{
	BreakType breakType = BreakType::Normal;

	switch (statement->type)
	{
		case ASTNodeType::CodeBlock:
			
			{
				PushLocalVarsOntoStack(statement->scopeBlock, stackFrameBeginning);

				for (int i = 0; i < statement->childs.size(); i++)
				{
					shared_ptr<ASTNode> child = statement->childs[i];

					breakType = EvaluateStatement(child, statement->scopeBlock, stackFrameBeginning);
					
					if (breakType != BreakType::Normal)
						break;
				}

				if (breakType != BreakType::Return)
				{
					PopLocalVarsFromStack(statement->scopeBlock, stackFrameBeginning);
				}
			}
			
			break;

		case ASTNodeType::IfStatement:
		case ASTNodeType::IfElseStatement:

			{
				Value condition = EvaluateExpression(statement->childs[0], scope, stackFrameBeginning);
				bool conditionExp;

				condition.AsBool(conditionExp);

				if (conditionExp)
					breakType = EvaluateStatement(statement->childs[1], scope, stackFrameBeginning);
				else if (statement->childs.size() == 3)
					breakType = EvaluateStatement(statement->childs[2], scope, stackFrameBeginning);
			}

			break;

		case ASTNodeType::AssignmentStatement:

			{
				Value rvalue = EvaluateExpression(statement->childs[1], scope, stackFrameBeginning);

				//handle complex lvalues
				shared_ptr<Variable> lvar = scope->FindVariable(statement->childs[0]->lexeme);
				
				if (lvar->type.IsEqualTo(&Figure))
					glDeleteTextures(1, &programStack[stackFrameBeginning + lvar->offset].data.figure);

				programStack[stackFrameBeginning + lvar->offset] = rvalue;
			}

			break;

		case ASTNodeType::WhileStatement:

			{
				Value condition;
				bool conditionExp;

				condition = EvaluateExpression(statement->childs[0], scope, stackFrameBeginning);
				condition.AsBool(conditionExp);

				while (conditionExp)
				{
					breakType = EvaluateStatement(statement->childs[1], scope, stackFrameBeginning);

					if (breakType == BreakType::Return)
						return BreakType::Return;
					else if (breakType == BreakType::Break)
						break;

					condition = EvaluateExpression(statement->childs[0], scope, stackFrameBeginning);
					condition.AsBool(conditionExp);
				}
			}

			break;

		case ASTNodeType::ReturnStatement:

			//pop everything(parameters and local vars) from stack and put back returnValue

			Value retValue;

			if (statement->childs.size() == 1)
			{
				//return it
				retValue = EvaluateExpression(statement->childs[0], scope, stackFrameBeginning);
			}

			programStack.erase(programStack.begin() + stackFrameBeginning, programStack.end());

			if (statement->childs.size() == 1)
			{
				programStack.push_back(retValue);
			}

			breakType = BreakType::Return;

			break;

	}

	return breakType;
}

Value Interpreter::EvaluateExpression(shared_ptr<ASTNode> expression, shared_ptr<Scope> scope, int stackFrameBeginning)
{
	Value result;

	switch (expression->type)
	{
		case ASTNodeType::Equals:
		case ASTNodeType::LessEquals:
		case ASTNodeType::GreaterEquals:
		case ASTNodeType::Less:
		case ASTNodeType::Greater:
		case ASTNodeType::NotEquals:
		case ASTNodeType::LogicalAnd:
		case ASTNodeType::LogicalOr:
		case ASTNodeType::Plus:
		case ASTNodeType::Minus:
		case ASTNodeType::Multiply:
		case ASTNodeType::Divide:

			{
				Value left = EvaluateExpression(expression->childs[0], scope, stackFrameBeginning);
				Value right = EvaluateExpression(expression->childs[1], scope, stackFrameBeginning);

				result = left.BinaryOperator(right, expression->type);
			}

			break;

		case ASTNodeType::NegatingExpression:

			result = EvaluateExpression(expression->childs[1], scope, stackFrameBeginning);
			result = result.UnaryOperator(expression->childs[0]->type);

			break;

		case ASTNodeType::Identifier:
			
			{
				//only for variables

				shared_ptr<Variable> var = scope->FindVariable(expression->lexeme);

				result = programStack[stackFrameBeginning + var->offset];
			}

			break;

		case ASTNodeType::ProcedureCallExpression:

			{
				shared_ptr<Procedure> proc;

				//this one is interesting :)
				switch (expression->childs[0]->type)
				{
					case ASTNodeType::Identifier:

						proc = scope->FindProcedure(expression->childs[0]->lexeme);

						break;
				}

				if (proc == createCircle)
				{
					float x = EvaluateExpression(expression->childs[1]->childs[0], scope, stackFrameBeginning).data.scalar;
					float y = EvaluateExpression(expression->childs[1]->childs[1], scope, stackFrameBeginning).data.scalar;
					float radius = EvaluateExpression(expression->childs[1]->childs[2], scope, stackFrameBeginning).data.scalar;

					result = Value::FromFigure(CreateCircle(x, y, radius));
				}
				else if (proc == createRect)
				{
					float x = EvaluateExpression(expression->childs[1]->childs[0], scope, stackFrameBeginning).data.scalar;
					float y = EvaluateExpression(expression->childs[1]->childs[1], scope, stackFrameBeginning).data.scalar;
					float width = EvaluateExpression(expression->childs[1]->childs[2], scope, stackFrameBeginning).data.scalar;
					float height = EvaluateExpression(expression->childs[1]->childs[3], scope, stackFrameBeginning).data.scalar;

					result = Value::FromFigure(CreateRect(x, y, width, height));
				}
				else if (proc == createTriangle)
				{
					float x1 = EvaluateExpression(expression->childs[1]->childs[0], scope, stackFrameBeginning).data.scalar;
					float y1 = EvaluateExpression(expression->childs[1]->childs[1], scope, stackFrameBeginning).data.scalar;
					float x2 = EvaluateExpression(expression->childs[1]->childs[2], scope, stackFrameBeginning).data.scalar;
					float y2 = EvaluateExpression(expression->childs[1]->childs[3], scope, stackFrameBeginning).data.scalar;
					float x3 = EvaluateExpression(expression->childs[1]->childs[4], scope, stackFrameBeginning).data.scalar;
					float y3 = EvaluateExpression(expression->childs[1]->childs[5], scope, stackFrameBeginning).data.scalar;

					result = Value::FromFigure(CreateTriangle(x1, y1, x2, y2, x3, y3));
				}
				else
				{
					//push params onto stack

					int newStackFrameBeginning = programStack.size();

					if (expression->childs.size() == 2)
					{
						for (int i = 0; i < expression->childs[1]->childs.size(); i++)
						{
							Value value = EvaluateExpression(expression->childs[1]->childs[i], scope, stackFrameBeginning);

							programStack.push_back(value);
						}
					}

					RunProcedure(proc, newStackFrameBeginning);

					if (proc->returnType.type != TypeType::Void)
					{
						result = programStack[newStackFrameBeginning];
					}
				}
			}

			break;

		case ASTNodeType::FloatLiteral:
			
			if (expression->typeValue.type == TypeType::Scalar)
			{
				float f = stof(expression->lexeme);

				result = Value::FromScalar(f);
			}

			break;
	}

	return result;
}

unsigned int Interpreter::CreateTriangle(float x1, float y1, float x2, float y2, float x3, float y3)
{
	unsigned int result;
	glGenTextures(1, &result);
	glBindTexture(GL_TEXTURE_2D, result);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, nullptr);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, Main::fbo);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result, 0);

	Main::drawFigureShader->Bind();

	glViewport(0, 0, 512, 512);
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_TRIANGLES);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glVertex2f(x3, y3);
	glEnd();

	return result;
}

unsigned int Interpreter::CreateRect(float x, float y, float width, float height)
{
	unsigned int result;
	glGenTextures(1, &result);
	glBindTexture(GL_TEXTURE_2D, result);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, nullptr);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, Main::fbo);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result, 0);

	Main::drawFigureShader->Bind();

	glViewport(0, 0, 512, 512);
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_TRIANGLE_STRIP);
	glVertex2f(x, y);
	glVertex2f(x, y - height);
	glVertex2f(x + width, y);
	glVertex2f(x + width, y - height);
	glEnd();

	return result;
}

unsigned int Interpreter::CreateCircle(float x, float y, float r)
{
	unsigned int result;
	glGenTextures(1, &result);
	glBindTexture(GL_TEXTURE_2D, result);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, nullptr);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, Main::fbo);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result, 0);

	Main::drawFigureShader->Bind();

	glViewport(0, 0, 512, 512);
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(x, y);

	for (int i = 0; i < 36; i++)
	{
		glVertex2f(x + r * cos(M_PI * 2 * i / 35.0f), y + r * sin(M_PI * 2 * i / 35.0f));
	}

	glEnd();

	return result;
}

void Interpreter::PushLocalVarsOntoStack(shared_ptr<Scope> scope, int stackFrameBeginning)
{
	for (int i = 0; i < scope->variables.size(); i++)
	{
		shared_ptr<Variable> var = scope->variables[i];

		if (var->initialValueExpression != nullptr)
		{
			Value value = EvaluateExpression(var->initialValueExpression, scope, stackFrameBeginning);

			programStack.push_back(value);
		}
		else
		{
			programStack.push_back(Value(var->type));
		}
	}
}

void Interpreter::PopLocalVarsFromStack(shared_ptr<Scope> scope, int stackFrameBeginning)
{
	programStack.erase(programStack.begin() + stackFrameBeginning + scope->offset, programStack.end());
}


void Interpreter::Interpret(shared_ptr<Program> program)
{
	shared_ptr<Procedure> mainProc = program->globalScope->FindProcedure("Main");

	if (!mainProc->returnType.IsEqualTo(&Figure))
	{
		cout << "Error: your main procedure must return resulting figure to be displayed." << endl;
		return;
	}

	RunProcedure(mainProc, 0);
	
	glDeleteTextures(1, &Main::resultTex);

	Main::resultTex = programStack[0].data.figure;
}

shared_ptr<Program> Interpreter::PrepareProgram(string filename)
{
	shared_ptr<Program> program = Semantics::ConstructProgram(filename);

	if (program != nullptr)
	{
		//add figure creation procedures
		createTriangle = make_shared<Procedure>();
		createTriangle->name = "CreateTriangle";
		createTriangle->isInternal = true;
		createTriangle->returnType = Figure;

		createRect = make_shared<Procedure>();
		createRect->name = "CreateRectangle";
		createRect->isInternal = true;
		createRect->returnType = Figure;

		createCircle = make_shared<Procedure>();
		createCircle->name = "CreateCircle";
		createCircle->isInternal = true;
		createCircle->returnType = Figure;

		program->globalScope->procedures.push_back(createTriangle);
		program->globalScope->procedures.push_back(createRect);
		program->globalScope->procedures.push_back(createCircle);

		Semantics::ExtractModuleParts(program);
		if (Semantics::ConstructModuleParts(program))
		{
			return program;
		}
	}

	return nullptr;
}