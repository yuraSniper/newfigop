#include "Value.h"

#include <Semantics\Program.h>
#include <Main\Main.h>

Value::Value()
{
}

Value::Value(Type type)
{
	this->type = type;

	data = ValueData();
}

unsigned int Value::CombineFigures(unsigned int left, unsigned int right, bool subtract)
{
	unsigned int result;
	glGenTextures(1, &result);
	glBindTexture(GL_TEXTURE_2D, result);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, Main::rectVbo);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, Main::fbo);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, left);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, right);

	Main::combineFiguresShader->Bind();
	glUniform1i(Main::combineFiguresShader->GetUniformLocation("subtract"), subtract);

	glViewport(0, 0, 512, 512);
	glClear(GL_COLOR_BUFFER_BIT);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	return result;
}

Value Value::BinaryOperator(Value other, ASTNodeType op)
{
	Value result;

	switch (op)
	{
		case ASTNodeType::Equals:
		
			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left == right);
				}
				else if (type.type == TypeType::Figure)
				{
					unsigned int left, right;
					AsFigure(left);
					other.AsFigure(right);

					result = Value::FromBool(left == right);
				}
			}

			break;

		case ASTNodeType::LessEquals:
			
			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left <= right);
				}
			}

			break;

		case ASTNodeType::GreaterEquals:
			
			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left >= right);
				}
			}

			break;

		case ASTNodeType::Less:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left < right);
				}
			}

			break;

		case ASTNodeType::Greater:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left > right);
				}
			}

			break;

		case ASTNodeType::NotEquals:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromBool(left != right);
				}
				else if (type.type == TypeType::Figure)
				{
					unsigned int left, right;
					AsFigure(left);
					other.AsFigure(right);

					result = Value::FromBool(left != right);
				}
			}

			break;

		case ASTNodeType::LogicalAnd:

			{
				bool left, right;
				AsBool(left);
				other.AsBool(right);

				result = Value::FromBool(left && right);
			}

			break;

		case ASTNodeType::LogicalOr:

			{
				bool left, right;
				AsBool(left);
				other.AsBool(right);

				result = Value::FromBool(left || right);
			}

			break;

		case ASTNodeType::Plus:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromScalar(left + right);
				}
				else if (type.type == TypeType::Figure)
				{
					unsigned int left, right;
					AsFigure(left);
					other.AsFigure(right);

					result = Value::FromFigure(CombineFigures(left, right, false));
				}
			}

			break;

		case ASTNodeType::Minus:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromScalar(left - right);
				}
				else if (type.type == TypeType::Figure)
				{
					unsigned int left, right;
					AsFigure(left);
					other.AsFigure(right);

					result = Value::FromFigure(CombineFigures(left, right, true));
				}
			}

			break;

		case ASTNodeType::Multiply:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromScalar(left * right);
				}
			}

			break;

		case ASTNodeType::Divide:

			{
				if (type.type == TypeType::Scalar)
				{
					float left, right;
					AsScalar(left);
					other.AsScalar(right);

					result = Value::FromScalar(left / right);
				}
			}

			break;
	}

	return result;
}

Value Value::UnaryOperator(ASTNodeType op, bool postfix)
{
	Value result;

	if (type.type == TypeType::Scalar)
	{
		float c;
		AsScalar(c);

		if (op == ASTNodeType::Minus)
		{
			result = Value::FromScalar(-c);
		}
	}

	return result;
}

bool Value::AsBool(bool & b)
{
	if (type.type == TypeType::Bool)
	{
		b = data.boolean;
		return true;
	}

	return false;
}

bool Value::AsScalar(float & f)
{
	if (type.type == TypeType::Scalar)
	{
		f = data.scalar;
		return true;
	}

	return false;
}

bool Value::AsFigure(unsigned int & f)
{
	if (type.type == TypeType::Figure)
	{
		f = data.figure;
		return true;
	}

	return false;
}


void Value::SetBool(bool b)
{
	(data.boolean) = b;
}

void Value::SetScalar(float f)
{
	(data.scalar) = f;
}

void Value::SetFigure(unsigned int f)
{
	(data.figure) = f;
}

Value Value::FromBool(bool b)
{
	Value result;
	result.type.type = TypeType::Bool;

	(result.data.boolean) = b;

	return result;
}

Value Value::FromScalar(float f)
{
	Value result;
	result.type.type = TypeType::Scalar;

	(result.data.scalar) = f;

	return result;
}

Value Value::FromFigure(unsigned int f)
{
	Value result;
	result.type.type = TypeType::Figure;

	(result.data.figure) = f;

	return result;
}

ValueData & ValueData::operator=(ValueData & other)
{
	figure = other.figure;

	return *this;
}

ValueData::ValueData()
{
}