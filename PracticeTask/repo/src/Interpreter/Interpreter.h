#pragma once

#include <common.h>
#include <Semantics\Semantics.h>
#include <Interpreter\Value.h>

enum class BreakType
{
	Normal,

	Return,
	Break,
	Continue,
};

class Interpreter
{
	void RunProcedure(shared_ptr<Procedure> procedure, int stackFrameBeginning);
	BreakType EvaluateStatement(shared_ptr<ASTNode> statement, shared_ptr<Scope> scope, int stackFrameBeginning);
	Value EvaluateExpression(shared_ptr<ASTNode> expression, shared_ptr<Scope> scope, int stackFrameBeginning);

	unsigned int CreateTriangle(float x1, float y1, float x2, float y2, float x3, float y3);
	unsigned int CreateRect(float x, float y, float width, float height);
	unsigned int CreateCircle(float x, float y, float r);

	void PushLocalVarsOntoStack(shared_ptr<Scope> scope, int stackFrameBeginning);
	void PopLocalVarsFromStack(shared_ptr<Scope> scope, int stackFrameBeginning);

	vector<Value> programStack;
	vector<string> stringLiterals;

	static shared_ptr<Procedure> createTriangle;
	static shared_ptr<Procedure> createRect;
	static shared_ptr<Procedure> createCircle;

public:

	void Interpret(shared_ptr<Program> program);

	static shared_ptr<Program> PrepareProgram(string filename);
};