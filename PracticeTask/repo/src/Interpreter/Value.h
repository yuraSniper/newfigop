#pragma once

#include <common.h>
#include <Semantics\Type.h>
#include <Parser\ASTNode.h>

class ValueData
{
public:
	union
	{
		float scalar;
		bool boolean;
		unsigned int figure;
	};

	ValueData();

	ValueData & operator=(ValueData & other);
};

class Value
{
public:
	Type type;
	ValueData data;

	Value();
	Value(Type type);

	unsigned int CombineFigures(unsigned int left, unsigned int right, bool subtract);
	Value BinaryOperator(Value other, ASTNodeType op);
	Value UnaryOperator(ASTNodeType op, bool postfix = false);

	bool AsBool(bool & b);
	bool AsScalar(float & f);
	bool AsFigure(unsigned int & f);

	void SetBool(bool b);
	void SetScalar(float f);
	void SetFigure(unsigned int f);

	static Value FromBool(bool b);
	static Value FromScalar(float f);
	static Value FromFigure(unsigned int f);
};