#include "Token.h"

vector<string> Token::types = {"Identifier", "Float", "Whitespace", "Unknown", "Other", "EOF"};

vector<string> tokens = {"return", "float", "while", "else", "func", "fig", "if",
"<=", ">=", "&&", "||", "==", "!=", "<", ">", "+", "-", "*", "/", "=", ",", ";", "(", ")", "{", "}"}; 

string Token::ToString()
{
	string result = "<";
	if ((int) type < tokens.size())
		result += tokens[(int) type];
	else
		result += types[(int) type - tokens.size()];

	result += ", \"" + lexeme + "\">";
	return result;
}