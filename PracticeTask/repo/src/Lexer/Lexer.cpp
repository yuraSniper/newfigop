#include "Lexer.h"

void Lexer::ReadChars(int count)
{
	char c = 0;
	eof = false;
	for (int i = 0; i < count - (int)lookaheadBuffer.length(); i++)
	{
		fileStream->get(c);

		if (fileStream->eof())
		{
			eof = true;
			return;
		}

		lookaheadBuffer.push_back(c);
	}
}

char Lexer::NextChar()
{
	return Lookahead(0);
}

char Lexer::Lookahead(int i)
{
	if (lookaheadBuffer.length() <= i)
	{
		ReadChars(i + 1);

		if (eof)
			return 0;
	}

	return lookaheadBuffer[i];
}

bool Lexer::MatchChars(string chars, int offset)
{
	ReadChars(chars.length() + offset);

	if (eof)
		return false;

	for (int i = 0; i < chars.length(); i++)
		if (Lookahead(i + offset) != chars[i])
			return false;

	return true;
}

void Lexer::Skip(int count)
{
	lookaheadBuffer.erase(0, count);
}

Token Lexer::MatchWhitespace()
{
	Token token;
	token.type = TokenTypes::Unknown;

	while (true)
	{
		char c = NextChar();
		if (eof)
			return token;
		
		if (c == '\n')
		{
			currentLine++;
			Skip();
		}
		else if (MatchChars("/*"))
		{
			int i = 2;
			while (!MatchChars("*/", i))
			{
				if (Lookahead(i) == '\n')
					currentLine++;

				i++;
				if (eof)
					break;
			}

			Skip(i + 2);
		}
		else if (MatchChars("//"))
		{
			int i = 2;
			while (Lookahead(i) != '\n')
			{
				i++;
				if (eof)
					break;
			}

			Skip(i);
		}
		else if (c != ' ' && c != '\t' && c != '\r')
		{
			break;
		}
		else
			Skip();
	}

	return token;
}

Token Lexer::MatchReservedToken(vector<string> & operators)
{
	Token token;
	token.type = TokenTypes::Unknown;

	for (int i = 0; i < operators.size(); i++)
	{
		if (MatchChars(operators[i]))
		{
			if (!(isalnum(operators[i][operators[i].length() - 1]) && isalnum(Lookahead(operators[i].length()))))
			{
				token.type = (TokenTypes) i;
				token.lexeme = operators[i];
				Skip(operators[i].length());

				break;
			}
		}
	}

	token.line = currentLine;

	return token;
}

Token Lexer::MatchIdentifier()
{
	Token token;
	token.type = TokenTypes::Unknown;
	token.line = currentLine;

	if (isalpha(NextChar()))
	{
		int i = 1;
		while (isalnum(Lookahead(i)))
		{
			if (eof)
			{
				token.type = TokenTypes::Eof;
				return token;
			}

			i++;
		}
		
		token.lexeme = lookaheadBuffer.substr(0, i);
		token.type = TokenTypes::Identifier;
		Skip(i);
	}

	return token;
}

Token Lexer::MatchNumber()
{
	Token token;
	token.type = TokenTypes::Unknown;
	token.line = currentLine;

	char c = Lookahead(0);

	if (isdigit(c))
	{
		int i = 1;

		if (c == '0' && (Lookahead(1) == 'x' || Lookahead(1) == 'X'))
		{
			i = 2;
			while (isxdigit(Lookahead(i)))
			{
				if (eof)
					break;

				i++;
			}

			token.type = TokenTypes::FloatLiteral;
		}
		else
		{
			bool dotPresent = false;
			while (true)
			{
				if (!isdigit(Lookahead(i)))
				{
					if (Lookahead(i) == '.')
					{
						if (dotPresent)
							return token;
						dotPresent = true;
					}
					else if (!isalpha(Lookahead(i)))
						break;
					else
						return token;
				}
				
				if (eof)
					break;

				i++;
			}

			token.type = TokenTypes::FloatLiteral;
		}

		if (isalpha(Lookahead(i)))
		{
			token.type = TokenTypes::Unknown;
			return token;
		}

		token.lexeme = lookaheadBuffer.substr(0, i);
		Skip(i);
	}

	return token;
}

Token Lexer::MatchOther()
{
	Token token;
	token.type = TokenTypes::Other;
	token.line = currentLine;

	while (true)
	{
		char c = NextChar();
		if (eof)
			break;

		if (c == ' ' || c == '\n' || c == '\r' || c == '\t')
			break;

		token.lexeme.push_back(c);
		Skip();
	}

	if (token.lexeme.length() == 0)
		token.type = TokenTypes::Unknown;

	return token;
}

Lexer::Lexer(string filename)
{
	fileStream = make_shared<ifstream>();
	fileStream->open(filename);
	this->filename = filename;
}

Token Lexer::NextToken()
{
	Token token;
	token.type = TokenTypes::Unknown;
	
	if (eof && lookaheadBuffer.length() == 0)
	{
		token.type = TokenTypes::Eof;
		return token;
	}

	eof = false;

	token = MatchWhitespace();
	if (token.type != TokenTypes::Unknown)
		return token;

	token = MatchReservedToken(tokens);
	if (token.type != TokenTypes::Unknown)
		return token;

	token = MatchIdentifier();
	if (token.type != TokenTypes::Unknown)
		return token;

	token = MatchNumber();
	if (token.type != TokenTypes::Unknown)
		return token;

	token = MatchOther();
	if (token.type != TokenTypes::Unknown)
		return token;

	if (eof && lookaheadBuffer.length() == 0)
	{
		token.type = TokenTypes::Eof;
		return token;
	}

	return token;
}