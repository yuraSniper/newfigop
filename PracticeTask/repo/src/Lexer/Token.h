#pragma once

#include <common.h>

extern vector<string> tokens;

enum class TokenTypes
{
	Return, Float, While, Else,
	Func, Fig, If, LessEquals, GreaterEquals,
	LogicalAnd, LogicalOr, Equals, NotEquals, Less, Greater, Plus, Minus, Multiply, Divide,
	Assign, Comma, Semicolon,
	OpenParentesis, CloseParentesis,
	OpenBrace, CloseBrace,

	Identifier,
	FloatLiteral,
	Whitespace,
	
	Unknown,
	Other,
	Eof,
};

struct Token
{
private:
	static vector<string> types;
public:
	string lexeme;
	TokenTypes type;
	int line;

	string ToString();
};