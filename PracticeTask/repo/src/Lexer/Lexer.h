#pragma once

#include <common.h>
#include <Lexer\Token.h>

class Lexer
{
	shared_ptr<ifstream> fileStream;
	string lookaheadBuffer;

	void ReadChars(int count = 1);
	char NextChar();
	char Lookahead(int i);

	bool MatchChars(string chars, int offset = 0);

	void Skip(int count = 1);

	Token MatchWhitespace();
	Token MatchReservedToken(vector<string> & operators);
	Token MatchIdentifier();
	Token MatchNumber();
	Token MatchOther();

	int currentLine = 1;
	bool eof = false;

public:
	string filename;
	Lexer(string filename);

	Token NextToken();
};