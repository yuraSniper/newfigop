#pragma once

#include <common.h>

const vector<tuple<string, int>> shaderTags = {
	make_tuple("#Vertex", GL_VERTEX_SHADER),
	make_tuple("#TessControl", GL_TESS_CONTROL_SHADER),
	make_tuple("#TessEval", GL_TESS_EVALUATION_SHADER),
	make_tuple("#Geometry", GL_GEOMETRY_SHADER),
	make_tuple("#Fragment", GL_FRAGMENT_SHADER),
	make_tuple("#Compute", GL_COMPUTE_SHADER)};

class ShaderProgram
{
	unsigned int id = 0;
	string file;

	ShaderProgram();
public:
	~ShaderProgram();

	static shared_ptr<ShaderProgram> CreateFromFile(string fileName);
	bool Reload();
	void Bind();

	unsigned int GetUniformLocation(string uniformname);
};