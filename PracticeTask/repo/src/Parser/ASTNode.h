#pragma once

#include <common.h>
#include <Lexer\Token.h>
#include <Semantics\Type.h>

enum class ASTNodeType
{
	Return, Float, While, Else,
	Func, Fig, If, LessEquals, GreaterEquals,
	LogicalAnd, LogicalOr, Equals, NotEquals, Less, Greater, Plus, Minus, Multiply, Divide,
	Assign, Comma, Semicolon,
	OpenParentesis, CloseParentesis,
	OpenBrace, CloseBrace,

	Identifier,
	FloatLiteral,
	Whitespace,

	Unknown,
	Other,
	Eof,
#include <Parser\Generated.h>
};

string GetSymbol(ASTNodeType type);

struct Scope;
struct Symbol;

class ASTNode
{
public:
	ASTNodeType type;
	string lexeme;
	int line;
	string filename;
	vector<shared_ptr<ASTNode>> childs;
	shared_ptr<ASTNode> parent;

	Type typeValue;
	shared_ptr<Symbol> symbol;
	shared_ptr<Scope> scopeBlock;

	ASTNode(shared_ptr<ASTNode> parent = nullptr)
	{
		this->parent = parent;
	}

	string ToString()
	{
		string result = GetSymbol(type);
		result += (lexeme.empty() ? "" : " \"" + lexeme + "\"");
		result += " :" + _Integral_to_string<char>(childs.size());

		/*if (typeValue != nullptr)
			result += " " + typeValue->ToString();*/

		return result;
	}

	shared_ptr<ASTNode> Clone()
	{
		shared_ptr<ASTNode> clone = make_shared<ASTNode>();

		clone->type = type;
		clone->lexeme = lexeme;
		clone->line = line;
		clone->filename = filename;
		clone->typeValue = typeValue;

		for (int i = 0; i < childs.size(); i++)
		{
			clone->childs.push_back(childs[i]->Clone());
			clone->childs[i]->parent = clone;
		}

		return clone;
	}
};