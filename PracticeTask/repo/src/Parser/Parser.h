#pragma once

#include <common.h>
#include <Lexer\Lexer.h>
#include <Lexer\Token.h>
#include <Parser\ASTNode.h>

enum class ActionType
{
	Error,
	Shift,
	Reduce,
	Accept,
	SRConflict,
	RRConflict,
};

struct Action
{
	ActionType action;
	int reduceByRule;
};

struct LR0Item
{
	int rule;
	int dot;
};

struct Rule
{
	int resultNonTerminal;
	int productionLength;
};

extern vector<string> Terminals;
extern vector<string> Nonterminals;
extern vector<Rule> Rules;
extern vector<vector<Action>> actionTable;
extern vector<vector<int>> gotoTable;

enum class ASTNodeType;

string GetSymbol(ASTNodeType type);

class Parser
{
	static shared_ptr<ASTNode> ReadNextToken(shared_ptr<Lexer> lexer);

public:
	static shared_ptr<ASTNode> Parse(string filename);
	static void Simplify(shared_ptr<ASTNode> root);
};