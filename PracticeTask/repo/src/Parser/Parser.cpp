#include "Parser.h"

#include <Parser\Generated.cpp>

string GetSymbol(ASTNodeType type)
{
	if ((int) type >= Terminals.size())
		return Nonterminals[(int) type - Terminals.size()];

	return Terminals[(int) type];
}

shared_ptr<ASTNode> Parser::ReadNextToken(shared_ptr<Lexer> lexer)
{
	Token token = lexer->NextToken();

	shared_ptr<ASTNode> nextToken = make_shared<ASTNode>(nullptr);
	nextToken->type = (ASTNodeType) token.type;
	nextToken->lexeme = token.lexeme;
	nextToken->line = token.line;
	nextToken->filename = lexer->filename;

	return nextToken;
}

shared_ptr<ASTNode> Parser::Parse(string filename)
{
	bool working = true;
	FILE * f = fopen(filename.c_str(), "r");
	if (f == nullptr)
		return nullptr;
	else
		fclose(f);

	shared_ptr<Lexer> lexer = make_shared<Lexer>(filename);
	stack<int> stateStack;
	stack<shared_ptr<ASTNode>> astNodeStack;
	shared_ptr<ASTNode> nextToken;

	stateStack.push(0);
	nextToken = ReadNextToken(lexer);

	while (working)
	{
		int state = stateStack.top();
		shared_ptr<ASTNode> newNode;
		
		while (nextToken->type == (ASTNodeType)TokenTypes::Whitespace)
			nextToken = ReadNextToken(lexer);

		switch (actionTable[(int) nextToken->type][state].action)
		{
			case ActionType::Accept:
				astNodeStack.top()->filename = filename;
				return astNodeStack.top();

			case ActionType::Error:
				cout << "Error: Unexpected token \"" << GetSymbol(nextToken->type) << "\" on line " << nextToken->line << " in \"" << filename << "\"" << endl;
				return nullptr;

			case ActionType::Reduce:
				newNode = make_shared<ASTNode>(nullptr);
				newNode->filename = filename;
				newNode->type = (ASTNodeType)Rules[actionTable[(int) nextToken->type][state].reduceByRule].resultNonTerminal;

				for (int i = 0; i < Rules[actionTable[(int) nextToken->type][state].reduceByRule].productionLength; i++)
				{
					newNode->childs.insert(newNode->childs.begin(), astNodeStack.top());
					astNodeStack.top()->parent = newNode;

					astNodeStack.pop();
					stateStack.pop();
				}

				if (newNode->childs.size() > 0)
					newNode->line = newNode->childs[0]->line;

				astNodeStack.push(newNode);
				state = stateStack.top();
				stateStack.push(gotoTable[(int) newNode->type][state]);

				break;

			case ActionType::SRConflict:
			case ActionType::Shift:
				astNodeStack.push(nextToken);
				stateStack.push(gotoTable[(int) nextToken->type][state]);
				nextToken = ReadNextToken(lexer);
				break;

			case ActionType::RRConflict:
				cout << "Your grammar sucks! Reduce-Reduce conflict for token \"" << GetSymbol(nextToken->type) << "\"" << endl;
				return nullptr;
		}
	}
}

void Parser::Simplify(shared_ptr<ASTNode> root)
{
	stack<pair<shared_ptr<ASTNode>, int>> list;
	list.push({root, 0});
	shared_ptr<ASTNode> lastVisited;

	while (!list.empty())
	{
		shared_ptr<ASTNode> current = list.top().first;
		shared_ptr<ASTNode> parent = current->parent;
		int childIndex = list.top().second;

		if (list.size() >= 2 && list._Get_container()[list.size() - 2].first != parent)
		{
			list.top().first->parent = list._Get_container()[list.size() - 2].first;
			parent = list.top().first->parent;
			//__asm int 3;
		}
		
		if (!current->childs.empty() && lastVisited == current->childs.back())
		{
			list.pop();
			lastVisited = current;

			if (parent != nullptr && parent->childs.size() > childIndex + 1)
				list.push({parent->childs[childIndex + 1], childIndex + 1});
		}
		else
		{
			lastVisited = current;
			bool isRemoved = false;

			if (parent != nullptr)
			{
				if (current->type == parent->type)
				{
					switch (current->type)
					{
						case ASTNodeType::MultiplicativeExpression:
						case ASTNodeType::AdditiveExpression:
						case ASTNodeType::BooleanExpression:
						case ASTNodeType::AdditiveOp:
						case ASTNodeType::BoolOp:
						case ASTNodeType::Plus:
						case ASTNodeType::Minus:
						case ASTNodeType::LogicalAnd:
						case ASTNodeType::LogicalOr:
						case ASTNodeType::Equals:
						case ASTNodeType::LessEquals:
						case ASTNodeType::GreaterEquals:
						case ASTNodeType::NotEquals:
						case ASTNodeType::Less:
						case ASTNodeType::Greater:
						case ASTNodeType::CodeBlock:
							break;

						default:
							parent->childs.erase(parent->childs.begin() + childIndex);
							parent->childs.insert(parent->childs.begin() + childIndex, current->childs.begin(), current->childs.end());

							for (int i = 0; i < current->childs.size(); i++)
								current->childs[i]->parent = parent;

							isRemoved = true;
							break;
					}
				}

				if (!isRemoved)
				{
					switch (current->type)
					{
						case ASTNodeType::While:
						case ASTNodeType::Else:
						case ASTNodeType::If:
						case ASTNodeType::Return:
						case ASTNodeType::Assign:
						case ASTNodeType::Comma:
						case ASTNodeType::Semicolon:
						case ASTNodeType::OpenParentesis:
						case ASTNodeType::CloseParentesis:
						case ASTNodeType::OpenBrace:
						case ASTNodeType::CloseBrace:

						case ASTNodeType::CodeBlockList:
						case ASTNodeType::CodeBlockWithStatement:
						case ASTNodeType::VariableDeclarationList:
						case ASTNodeType::ProcedureList:

							parent->childs.erase(parent->childs.begin() + childIndex);
							parent->childs.insert(parent->childs.begin() + childIndex, current->childs.begin(), current->childs.end());

							for (int i = 0; i < current->childs.size(); i++)
								current->childs[i]->parent = parent;

							isRemoved = true;
							break;

						case ASTNodeType::BoolOp:
						case ASTNodeType::AdditiveOp:
						case ASTNodeType::MultiplicativeOp:

							parent->childs.erase(parent->childs.begin() + childIndex);
							parent->childs.insert(parent->childs.begin() + childIndex, current->childs[0]);

							current->childs[0]->childs.insert(parent->childs[childIndex]->childs.begin(), current->childs.begin() + 1, current->childs.end());
							current->childs[0]->parent = parent;

							for (int i = 1; i < current->childs.size(); i++)
								current->childs[i]->parent = current->childs[0];

							isRemoved = true;
							break;
					}
				}

				if (!isRemoved && current->childs.size() == 1)
				{
					switch (current->type)
					{
						case ASTNodeType::Program:
						case ASTNodeType::Module:
						case ASTNodeType::ReturnStatement:
						case ASTNodeType::ProcedureArgumentList:
						case ASTNodeType::ProcedureParameterList:
						case ASTNodeType::CodeBlock:
						case ASTNodeType::VariableDeclaration:
						case ASTNodeType::ProcedureCallExpression:

							break;

						default:
							parent->childs.erase(parent->childs.begin() + childIndex);
							parent->childs.insert(parent->childs.begin() + childIndex, current->childs.begin(), current->childs.end());

							for (int i = 0; i < current->childs.size(); i++)
								current->childs[i]->parent = parent;

							isRemoved = true;
							break;
					}
				}
				else if (!isRemoved && current->childs.size() == 3)
				{
					shared_ptr<ASTNode> op = current->childs[1];

					switch (current->type)
					{
						case ASTNodeType::MultiplicativeExpression:
						case ASTNodeType::AdditiveExpression:
						case ASTNodeType::BooleanExpression:

							parent->childs.erase(parent->childs.begin() + childIndex);
							parent->childs.insert(parent->childs.begin() + childIndex, op);
							op->childs.push_back(current->childs[0]);
							op->childs.push_back(current->childs[2]);

							op->parent = parent;
							current->childs[0]->parent = op;
							current->childs[2]->parent = op;

							isRemoved = true;
							break;
					}
				}
			}

			if (!isRemoved && !current->childs.empty())
				list.push({current->childs[0], 0});
			else
			{
				list.pop();
				int nextChildIndex = isRemoved ? childIndex : childIndex + 1;

				if (parent != nullptr && parent->childs.size() > nextChildIndex)
					list.push({parent->childs[nextChildIndex], nextChildIndex});
			}
		}
	}
}