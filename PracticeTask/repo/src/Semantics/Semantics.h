#pragma once

#include <common.h>
#include <Parser\Parser.h>
#include <Semantics\Program.h>

class Semantics
{
	static shared_ptr<Procedure> ExtractProcedure(shared_ptr<Scope> scope, shared_ptr<ASTNode> procedureNode);
	static bool ConstructProcedure(shared_ptr<Procedure> procedure);

	static int GetNextVariableOffset(shared_ptr<Scope> scope);

	static bool InferExpression(shared_ptr<Scope> scope, shared_ptr<ASTNode> expression);
	static bool InferStatement(shared_ptr<Scope> scope, shared_ptr<ASTNode> statement);

	static Type ExtractTypeFromNode(shared_ptr<Scope> scope, shared_ptr<ASTNode> typeNode);
	static vector<shared_ptr<Variable>> ExtractVariables(shared_ptr<Scope> scope, shared_ptr<ASTNode> statement);

	static bool IsTypeEqualToAnyOf(Type type, vector<Type> otherTypes);

	static void Error(string message, int line, string file, bool printNewLine = true);

public:
	static shared_ptr<Program> ConstructProgram(string filename);
	static void ExtractModuleParts(shared_ptr<Program> program);
	static bool ConstructModuleParts(shared_ptr<Program> program);
};