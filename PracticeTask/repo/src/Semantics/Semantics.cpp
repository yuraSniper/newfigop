#include "Semantics.h"

shared_ptr<Program> Semantics::ConstructProgram(string filename)
{
	shared_ptr<Program> program = make_shared<Program>();

	program->ast = make_shared<ASTNode>();
	program->ast->type = ASTNodeType::Program;
	program->globalScope = make_shared<Scope>();
	program->globalScope->name = "global";
	program->globalScope->type = ScopeType::GlobalScope;

	vector<string> filelist = {filename};
	unordered_set<string> files = {filename};

	while (!filelist.empty())
	{
		shared_ptr<ASTNode> module;
		module = Parser::Parse(filelist[0]);
		if (module != nullptr)
		{
			Parser::Simplify(module);
			program->ast->childs.push_back(module);
			module->parent = program->ast;
		}
		else
		{
			cout << "Error: File \"" << filelist[0] << "\" doesn't exist." << endl;
			return nullptr;
		}

		filelist.erase(filelist.begin());
	}

	//Parser::Simplify(program->ast);

	return program;
}

void Semantics::ExtractModuleParts(shared_ptr<Program> program)
{
	shared_ptr<ASTNode> root = program->ast->childs[0];

	for (int partIndex = 0; partIndex < root->childs.size(); partIndex++)
	{
		shared_ptr<ASTNode> child = root->childs[partIndex];
		shared_ptr<Procedure> procedure = ExtractProcedure(program->globalScope, child);
		program->globalScope->procedures.push_back(procedure);
	}
}

bool Semantics::ConstructModuleParts(shared_ptr<Program> program)
{
	bool errorsPresent = false;

	shared_ptr<Procedure> mainProc = program->globalScope->FindProcedure("Main");

	if (mainProc == nullptr)
	{
		Error("Your program doesn't have a Main function", -1, "");
		return false;
	}

	for (int i = 0; i < program->globalScope->procedures.size(); i++)
	{
		if (!program->globalScope->procedures[i]->isInternal)
			errorsPresent |= ConstructProcedure(program->globalScope->procedures[i]);
	}

	return !errorsPresent;
}

shared_ptr<Procedure> Semantics::ExtractProcedure(shared_ptr<Scope> scope, shared_ptr<ASTNode> procedureNode)
{
	shared_ptr<Procedure> procedure = make_shared<Procedure>();
	procedure->header = procedureNode->childs[0];
	procedure->body = procedureNode->childs[1];
	procedure->name = procedureNode->childs[0]->childs[1]->lexeme;
	
	procedure->scope = make_shared<Scope>();
	procedure->scope->type = ScopeType::ProcedureScope;
	procedure->scope->offset = 0;
	procedure->scope->parentScope = scope;
	procedure->scope->name = procedure->name;
	
	return procedure;
}

bool Semantics::ConstructProcedure(shared_ptr<Procedure> procedure)
{
	bool errorsPresent = false;

	procedure->returnType = ExtractTypeFromNode(procedure->scope->parentScope, procedure->header->childs[0]);

	if (procedure->header->childs.size() > 2)
	{
		shared_ptr<ASTNode> arguments = procedure->header->childs[2];

		for (int i = 0; i < arguments->childs.size(); i++)
		{
			shared_ptr<ASTNode> argument = arguments->childs[i];
			shared_ptr<ASTNode> typeNode = argument->childs[0];
			Type type = ExtractTypeFromNode(procedure->scope->parentScope, typeNode);

			string argumentName = argument->childs[1]->lexeme;
			shared_ptr<Variable> argumentVariable = make_shared<Variable>();
			argumentVariable->name = argumentName;
			argumentVariable->offset = GetNextVariableOffset(procedure->scope);
			argumentVariable->scope = procedure->scope;
			argumentVariable->type = type;

			procedure->scope->variables.push_back(argumentVariable);
			procedure->arguments.push_back(argumentVariable);
		}
	}

	errorsPresent |= InferStatement(procedure->scope, procedure->body);

	return errorsPresent;
}

int Semantics::GetNextVariableOffset(shared_ptr<Scope> scope)
{
	//scope is MethodScope, ProcedureScope or BlockScope
	//TODO(yura): change this to a byte offset
	int offset = 0;

	if (scope->variables.empty())
		offset = scope->offset;
	else
		offset = scope->variables.back()->offset + 1;

	return offset;
}

bool Semantics::InferExpression(shared_ptr<Scope> scope, shared_ptr<ASTNode> expression)
{
	bool errorsPresent = false;

	switch (expression->type)
	{
		case ASTNodeType::Less:
		case ASTNodeType::LessEquals:
		case ASTNodeType::Greater:
		case ASTNodeType::GreaterEquals:
		case ASTNodeType::Equals:
		case ASTNodeType::NotEquals:
			
			errorsPresent |= InferExpression(scope, expression->childs[0]);
			errorsPresent |= InferExpression(scope, expression->childs[1]);

			if (!errorsPresent)
			{
				Type typeToTest = expression->childs[0]->typeValue;
				Type typeToTest2 = expression->childs[1]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown || typeToTest2.type == TypeType::Unknown)
					return false;

				if (!IsTypeEqualToAnyOf(typeToTest, {Float}) && typeToTest.IsEqualTo(&typeToTest2))
				{
					Error("It is not possible to apply '" + expression->lexeme + "' operator on non scalar types", expression->line, expression->filename);
					return true;
				}
				else
				{
					//TODO: handle custom overridden comparison operators
				}

				expression->typeValue = Bool;
			}

			break;

		case ASTNodeType::LogicalAnd:
		case ASTNodeType::LogicalOr:

			errorsPresent |= InferExpression(scope, expression->childs[0]);
			errorsPresent |= InferExpression(scope, expression->childs[1]);

			if (!errorsPresent)
			{
				Type typeToTest = expression->childs[0]->typeValue;
				Type typeToTest2 = expression->childs[1]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown || typeToTest2.type == TypeType::Unknown)
					return false;

				if (!typeToTest.IsEqualTo(&Bool) ||
					!typeToTest2.IsEqualTo(&Bool))
				{
					Error("It is not possible to 'or(||)' or 'and(&&)' non boolean expressions (and no, you cannot override boolean '||' or '&&' operators)", expression->line, expression->filename);
					return true;
				}

				expression->typeValue = Bool;
			}

			break;

		case ASTNodeType::Plus:
		case ASTNodeType::Minus:
		case ASTNodeType::Multiply:
		case ASTNodeType::Divide:

			errorsPresent |= InferExpression(scope, expression->childs[0]);
			errorsPresent |= InferExpression(scope, expression->childs[1]);

			if (!errorsPresent)
			{
				Type typeToTest = expression->childs[0]->typeValue;
				Type typeToTest2 = expression->childs[1]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown || typeToTest2.type == TypeType::Unknown)
					return false;

				if (!typeToTest.IsEqualTo(&typeToTest2))
				{
					if (false)
					{
						//TODO: handle custom overridden arithmetic operators
					}
					else
					{
						Error("It isn't possible to apply arithmetic operator on non-compatible types (\"" + typeToTest.ToString() + "\" and \"" + typeToTest2.ToString() + "\")", expression->line, expression->filename);
						return true;
					}
				}

				expression->typeValue = typeToTest;
			}

			break;

		case ASTNodeType::NegatingExpression:

			errorsPresent |= InferExpression(scope, expression->childs[1]);

			if (!errorsPresent)
			{
				Type typeToTest = expression->childs[1]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown)
					return false;

				if (!IsTypeEqualToAnyOf(typeToTest, {Float}))
				{
					//TODO: handle custom overridden negating operator
					Error("It isn't possible to negate a non integer value", expression->line, expression->filename);
					return true;
				}

				expression->typeValue = typeToTest;
			}

			break;

		case ASTNodeType::ProcedureCallExpression:

			{
				shared_ptr<Procedure> procedure;

				if (expression->childs[0]->type == ASTNodeType::Identifier)
				{
					procedure = scope->FindProcedure(expression->childs[0]->lexeme);

					if (procedure == nullptr)
					{
						Error("It is not possible to call undefined procedure \"" + expression->childs[0]->lexeme + "\"", expression->line, expression->filename);
						return true;
					}
				}
				else
				{
					Error("Hey dude! What ya smokin'? Stop it!", expression->line, expression->filename);
					return true;
				}

				if (procedure != nullptr)
				{
					auto & params = procedure->arguments;

					int paramCount = 0;
					if (expression->childs.size() == 2)
						paramCount = expression->childs[1]->childs.size();

					if (!procedure->isInternal && params.size() != paramCount)
					{
						Error("It is not possible to call a procedure with " + ((paramCount > params.size()) ? string("more") : "less") + " parameters than it accepts. (" + _Integral_to_string<char>(paramCount) + " given, " + _Integral_to_string<char>(params.size()) + " expected)", expression->line, expression->filename);
						return true;
					}

					if (expression->childs.size() == 2)
					{
						//checking for args types to match
						shared_ptr<ASTNode> args = expression->childs[1];

						for (int i = 0; i < args->childs.size(); i++)
						{
							errorsPresent |= InferExpression(scope, args->childs[i]);

							if (!errorsPresent)
							{
								Type type = args->childs[i]->typeValue;

								//Previous error caused type == null, so we can "safely ignore" this
								if (type.type == TypeType::Unknown)
									return false;

								if (!procedure->isInternal && !type.IsEqualTo(&params[i]->type))
								{
									Error("It is not possible to cast parameter " + _Integral_to_string<char>(i + 1) + " \"" + type.ToString() + "\" to \"" + params[i]->type.ToString() + "\"", args->childs[i]->line, args->childs[i]->filename);
									return true;
								}
							}
						}
					}
				}

				if (!errorsPresent)
				{
					expression->typeValue = procedure->returnType;
				}
			}

			break;

		case ASTNodeType::Identifier:

			{
				shared_ptr<Symbol> symbol = scope->FindSymbol(expression->lexeme);
				
				if (symbol == nullptr)
				{
					Error("It is not possible to use undefined symbol \"" + expression->lexeme + "\"", expression->line, expression->filename);
					return true;
				}
				
				if (symbol->symbolType == SymbolType::Variable)
					expression->typeValue = symbol->type;
				else
					expression->symbol = symbol;
			}

			break;

		case ASTNodeType::FloatLiteral:

			//TODO(yura): introduce some kind of generic float type
			expression->typeValue = Float;

			break;

		default:
			Error("Compiler bug: Expression \"" + expression->ToString() + "\" isn't handled!", expression->line, expression->filename);
			return true;
	}

	return errorsPresent;
}

bool Semantics::InferStatement(shared_ptr<Scope> scope, shared_ptr<ASTNode> statement)
{
	bool errorsPresent = false;

	switch (statement->type)
	{
		case ASTNodeType::VariableDeclarationStatement:
			{
				vector<shared_ptr<Variable>> variables = ExtractVariables(scope, statement);

				int offset = GetNextVariableOffset(scope);

				for (int i = 0; i < variables.size(); i++)
				{
					shared_ptr<Variable> var = variables[i];

					//TODO(yura): change this to a byte offset
					var->offset = offset;
					offset++;

					if (var->initialValueExpression != nullptr)
					{
						errorsPresent |= InferExpression(scope, var->initialValueExpression);

						if (!errorsPresent)
						{
							Type typeToTest = var->initialValueExpression->typeValue;

							//Previous error caused type == null, so we can "safely ignore" this
							if (typeToTest.type == TypeType::Unknown || var->type.type == TypeType::Unknown)
								return false;

							if (!typeToTest.IsEqualTo(&var->type))
							{
								Error("It is not possible to assign a variable value with different type", variables[i]->initialValueExpression->line, variables[i]->initialValueExpression->filename);
								return true;
							}
						}
					}
				}

				if (variables.empty())
					return true;

				scope->variables.insert(scope->variables.end(), variables.begin(), variables.end());
			}
			break;

		case ASTNodeType::CodeBlock:

			if (statement->scopeBlock == nullptr)
			{
				statement->scopeBlock = make_shared<Scope>();
				statement->scopeBlock->type = ScopeType::BlockScope;
				statement->scopeBlock->parentScope = scope;
			}
			
			if (statement->scopeBlock->type == ScopeType::BlockScope)
				statement->scopeBlock->offset = GetNextVariableOffset(scope);
			
			for (int i = 0; i < statement->childs.size(); i++)
			{
				shared_ptr<ASTNode> stmt = statement->childs[i];

				errorsPresent |= InferStatement(statement->scopeBlock, stmt);
			}

			break;

		case ASTNodeType::IfStatement:
			errorsPresent |= InferExpression(scope, statement->childs[0]);

			if (!errorsPresent)
			{
				Type typeToTest = statement->childs[0]->typeValue;
				
				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown)
					return false;

				if (!typeToTest.IsEqualTo(&Bool))
				{
					Error("Condition expression type is not bool", statement->line, statement->filename);
					return true;
				}

				errorsPresent |= InferStatement(scope, statement->childs[1]);
			}
			break;

		case ASTNodeType::IfElseStatement:
			errorsPresent |= InferExpression(scope, statement->childs[0]);

			if (!errorsPresent)
			{
				Type typeToTest = statement->childs[0]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown)
					return false;

				if (!typeToTest.IsEqualTo(&Bool))
				{
					Error("Condition expression type is not bool", statement->line, statement->filename);
					return true;
				}

				errorsPresent |= InferStatement(scope, statement->childs[1]);
				errorsPresent |= InferStatement(scope, statement->childs[2]);
			}
			break;

		case ASTNodeType::AssignmentStatement:
			errorsPresent |= InferExpression(scope, statement->childs[0]);
			errorsPresent |= InferExpression(scope, statement->childs[1]);

			if (!errorsPresent)
			{
				Type typeToTest = statement->childs[0]->typeValue;
				Type typeToTest2 = statement->childs[1]->typeValue;

				//Previous error caused type == null, so we can "safely ignore" this
				if (typeToTest.type == TypeType::Unknown || typeToTest2.type == TypeType::Unknown)
					return false;

				if (!typeToTest.IsEqualTo(&typeToTest2))
				{
					Error("It is not possible to assign a variable value with different type", statement->line, statement->filename);
					return true;
				}
			}

			break;

		case ASTNodeType::ReturnStatement:

			{
				Type typeToTest;
			
				if (statement->childs.size() == 1)
				{
					errorsPresent |= InferExpression(scope, statement->childs[0]);
					typeToTest = statement->childs[0]->typeValue;
				}
				else
				{
					typeToTest = Void;
				}

				if (!errorsPresent)
				{
					//Previous error caused type == null, so we can "safely ignore" this
					if (typeToTest.type == TypeType::Unknown)
						return false;

					while (scope != nullptr && scope->name == "")
					{
						scope = scope->parentScope;
					}

					if (scope != nullptr && scope->parentScope != nullptr)
					{
						shared_ptr<Procedure> proc = scope->parentScope->FindProcedure(scope->name);

						if (proc != nullptr)
						{
							if (typeToTest.IsEqualTo(&proc->returnType))
								break;
							else
							{
								Error("Return value type doesn't match procedure return type", statement->line, statement->filename);
								return true;
							}
						}
					}

					Error("It is not possible to use return keyword outside of a procedure, because it is very dangerous!", statement->line, statement->filename);
					return true;
				}
			}

			break;

		case ASTNodeType::WhileStatement:
			errorsPresent |= InferExpression(scope, statement->childs[0]);
			errorsPresent |= InferStatement(scope, statement->childs[1]);
			break;

		default:
			Error("Compiler bug: Statement \"" + statement->ToString() + "\" isn't handled!", statement->line, statement->filename);
			return true;
	}

	return errorsPresent;
}

Type Semantics::ExtractTypeFromNode(shared_ptr<Scope> scope, shared_ptr<ASTNode> typeNode)
{
	Type type;
	switch (typeNode->type)
	{
		case ASTNodeType::Func:

			type = Void;
			break;

		case ASTNodeType::Float:

			type = Float;
			break;

		case ASTNodeType::Fig:

			type = Figure;
			break;
	}

	return type;
}

vector<shared_ptr<Variable>> Semantics::ExtractVariables(shared_ptr<Scope> scope, shared_ptr<ASTNode> statement)
{
	vector<shared_ptr<Variable>> variables;
	Type type;

	int child = 0;

	type = ExtractTypeFromNode(scope, statement->childs[child++]);

	if (type.type == TypeType::Unknown)
	{
		Error("It is not possible to use undefined type", statement->line, statement->filename);
		return variables;
	}

	for (int i = child; i < statement->childs.size(); i++)
	{
		shared_ptr<Variable> var = make_shared<Variable>();
		var->name = statement->childs[i]->childs[0]->lexeme;
		if (statement->childs[i]->childs.size() > 1)
			var->initialValueExpression = statement->childs[i]->childs[1];

		var->scope = scope;
		var->type = type;

		variables.push_back(var);
	}

	return variables;
}

bool Semantics::IsTypeEqualToAnyOf(Type type, vector<Type> otherTypes)
{
	for (int i = 0; i < otherTypes.size(); i++)
		if (type.IsEqualTo(&otherTypes[i]))
			return true; 

	return false;
}

void Semantics::Error(string message, int line, string file, bool printNewLine)
{
	cout << "Error: " << message;
	
	if (line != -1)
		cout << " on line " << line << " in " << file;

	if (printNewLine)
		cout << endl;
}