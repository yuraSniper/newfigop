#pragma once

#include <common.h>
#include <Parser\Parser.h>
#include <Semantics\Type.h>

class ASTNode;

enum class SymbolType
{
	Variable,
	Procedure,
};

enum class ScopeType
{
	ProcedureScope,
	BlockScope,
	GlobalScope,
};

struct Variable;
struct Procedure;
struct Symbol;
struct Scope;

struct Scope
{
	ScopeType type;
	string name;

	//this is offset of the first variable in this scope
	int offset;

	shared_ptr<Scope> parentScope;
	//vector<shared_ptr<Scope>> scopes;
	vector<shared_ptr<Variable>> variables;
	vector<shared_ptr<Procedure>> procedures;

	vector<shared_ptr<Procedure>> FindProcedures(string name, bool recursive = true);

	shared_ptr<Symbol> FindSymbol(string name, bool recursive = true);
	shared_ptr<Procedure> FindProcedure(string name, bool recursive = true);
	shared_ptr<Variable> FindVariable(string name, bool recursive = true);
};

struct Symbol
{
	string name;
	bool isStatic = false;
	shared_ptr<Scope> scope;

	SymbolType symbolType;

	Type type;
};

struct Variable : public Symbol
{
	shared_ptr<ASTNode> initialValueExpression;

	//in procedure this is index
	//in structure this is byte offset
	int offset;

	Variable()
	{
		this->symbolType = SymbolType::Variable;
	}
};

struct Procedure : public Symbol
{
	shared_ptr<ASTNode> header;
	shared_ptr<ASTNode> body;
	vector<shared_ptr<Variable>> arguments;
	Type returnType;

	//TODO: remove! this is a hack for not having a polymorphic procedures
	bool isInternal = false;

	Procedure()
	{
		this->symbolType = SymbolType::Procedure;
	}
};

struct Program
{
	shared_ptr<ASTNode> ast;

	shared_ptr<Scope> globalScope;
};