#pragma once

#include <common.h>

struct Structure;

enum class TypeType
{
	Unknown,

	Scalar,
	Figure,
	Bool,
	Void,
};

class Type
{
	void Init(TypeType type);
public:
	TypeType type;

	Type();
	Type(TypeType type);
	Type(const Type & t);
	Type & operator=(Type & t);
	~Type();

	bool IsEqualTo(Type * other);

	int GetSize();
	string ToString();
};

extern Type Float;
extern Type Figure;
extern Type Bool;
extern Type Void;