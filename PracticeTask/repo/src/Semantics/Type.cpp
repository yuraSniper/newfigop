#include "Type.h"

#include <Semantics\Program.h>

Type Float(TypeType::Scalar);
Type Figure(TypeType::Figure);
Type Bool(TypeType::Bool);
Type Void(TypeType::Void);

void Type::Init(TypeType type)
{
	this->type = type;
}

Type::Type()
{

}

Type::Type(TypeType type)
{
	Init(type);
}

Type::Type(const Type & t)
{
	*this = (Type &)t;
}

Type & Type::operator=(Type & t)
{
	type = t.type;

	return *this;
}

Type::~Type()
{
}

bool Type::IsEqualTo(Type * other)
{
	if (type != other->type)
		return false;

	return true;
}

int Type::GetSize()
{
	int size = 0;

	if (type == TypeType::Scalar)
		size = 4;
	else if (type == TypeType::Figure)
		size = 4;
	else if (type == TypeType::Bool)
		size = 1;

	return size;
}

string Type::ToString()
{
	return "";
}