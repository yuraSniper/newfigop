#include "Program.h"

vector<shared_ptr<Procedure>> Scope::FindProcedures(string name, bool recursive)
{
	vector<shared_ptr<Procedure>> procs;

	for (int i = 0; i < procedures.size(); i++)
		if (procedures[i]->name == name)
			procs.push_back(procedures[i]);

	if (parentScope != nullptr && recursive)
	{
		vector<shared_ptr<Procedure>> p = parentScope->FindProcedures(name);
		procs.insert(procs.end(), p.begin(), p.end());
	}

	return procs;
}

shared_ptr<Symbol> Scope::FindSymbol(string name, bool recursive)
{
	shared_ptr<Symbol> symbol = FindVariable(name);
	
	if (symbol == nullptr)
		symbol = FindProcedure(name);
	
	if (symbol == nullptr && recursive)
	{
		if (parentScope != nullptr)
			return parentScope->FindSymbol(name);
	}

	return symbol;
}

shared_ptr<Procedure> Scope::FindProcedure(string name, bool recursive)
{
	for (int i = 0; i < procedures.size(); i++)
		if (procedures[i]->name == name)
			return procedures[i];

	if (parentScope != nullptr && recursive)
		return parentScope->FindProcedure(name);

	return nullptr;
}

shared_ptr<Variable> Scope::FindVariable(string id, bool recursive)
{
	for (int i = 0; i < variables.size(); i++)
		if (variables[i]->name == id)
			return variables[i];

	if (parentScope != nullptr && recursive)
		return parentScope->FindVariable(id);

	return nullptr;
}