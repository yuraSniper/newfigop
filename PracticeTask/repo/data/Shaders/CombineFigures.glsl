#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec2 pos;
layout(location = 0) in vec2 texCoord;

smooth out vec2 vTexCoord;

void main()
{
	gl_Position = mvp * vec4(pos, 0, 1);
	vTexCoord = texCoord;
}

#Fragment
#version 330

uniform sampler2D left;
uniform sampler2D right;
uniform bool subtract;

smooth in vec2 vTexCoord;

layout(location = 0) out float outResult;

float invert(float a)
{
	return 1 - a;
}

float intersect(float l, float r)
{
	return l * r;
}

float unionV(float l, float r)
{
	return invert(intersect(invert(l), invert(r)));
}

void main()
{
	float leftValue = texture(left, vTexCoord).r;
	float rightValue = texture(right, vTexCoord).r;

	float unionResult = unionV(leftValue, rightValue);
	float subtractResult = intersect(leftValue, invert(rightValue));

	outResult = mix(unionResult, subtractResult, subtract);
}