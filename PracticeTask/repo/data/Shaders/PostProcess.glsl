#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 texCoord;

smooth out vec2 vPos;
smooth out vec2 vTexCoord;

void main()
{
	gl_Position = mvp * vec4(pos, 0, 1);
	vTexCoord = texCoord;
}

#Fragment
#version 330

uniform sampler2D resultTex;

smooth in vec2 vTexCoord;

layout(location = 0) out vec4 outColor;

void main()
{
	outColor = mix(vec4(0, 0, 0, 1), vec4(0, 1, 0, 1), texture(resultTex, vTexCoord).r);
}